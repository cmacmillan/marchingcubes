using UnityEngine;

public class CornerConfiguration
{
    public int cachedIndex;
    public int index {
        get {
            int retr=0;
            for (int i=0;i<8;i++){
                if (Corners[i]){
                    retr|=1<<i;
                }
            }
            return retr;
        }
    }
    private bool[] _corners;
    public bool[] Corners {
        get 
        {
            if (_corners==null){
                _corners = new bool[8];
            }
            return _corners;
        }
    }
    public struct EdgeCycle{
        private int mod(int x, int m)
        {
            return (x % m + m) % m;
        }
        public int this[int i]
        {
            get {
                i=mod(i,4);
                switch(i){
                    case 0:
                        return index0;
                    case 1:
                        return index1;
                    case 2:
                        return index2;
                    case 3:
                    default:
                        return index3;
                }
            }
        }
        public EdgeCycle(int i0,int i1, int i2,int i3)
        {
            index0=i0;
            index1=i1;
            index2=i2;
            index3=i3;
        }
        int index0;
        int index1;
        int index2;
        int index3;
    }
    public void ShiftEdgeCycle(EdgeCycle cycle){
        bool storedVal=false;
        for (int i=0;i<4;i++){
            if (i==0){
                storedVal=Corners[cycle[i]];
                Corners[cycle[i]]=Corners[cycle[i-1]];
            } else {
                var newStoredVal =Corners[cycle[i]];
                Corners[cycle[i]]=storedVal;
                storedVal = newStoredVal;
            }
        }
    }

    private static EdgeCycle ccZcycle1 = new EdgeCycle(0,1,3,2);
    private static EdgeCycle ccZcycle2 = new EdgeCycle(4,5,7,6);
    private static EdgeCycle ccXcycle1 = new EdgeCycle(5,1,3,7);
    private static EdgeCycle ccXcycle2 = new EdgeCycle(4,0,2,6);
    private static EdgeCycle ccYcycle1 = new EdgeCycle(0,1,5,4);
    private static EdgeCycle ccYcycle2 = new EdgeCycle(2,3,7,6);
    public void rotateCounterClockwiseAroundZ(){ ShiftEdgeCycle(ccZcycle1);ShiftEdgeCycle(ccZcycle2);}
    public void rotateCounterClockwiseAroundX(){ ShiftEdgeCycle(ccXcycle1);ShiftEdgeCycle(ccXcycle2);}
    public void rotateCounterClockwiseAroundY(){ ShiftEdgeCycle(ccYcycle1);ShiftEdgeCycle(ccYcycle2);}
    public int NumNodes(){
        int total=0;
        for (int i=0;i<8;i++){
            if (Corners[i]){
                total++;
            }
        }
        return total;
    }
    public void Invert(){
        for (int i=0;i<8;i++){
            Corners[i] = !Corners[i];
        }
    }
    public CornerConfiguration(int index){
        InitializeFromIndex(index);
    }

    public override bool Equals(object obj){
        if (!(obj is CornerConfiguration)){
            return false;
        }
        CornerConfiguration other =(CornerConfiguration)obj;
        for (int i=0;i<8;i++){
            if (other.Corners[i]!=Corners[i]){
                return false;
            }
        }
        return true;
    }
    public void InitializeFromIndex(int index){
        for (int i=0;i<8;i++){
            Corners[i]=(index&(1<<i))!=0;
        }
    }
    public bool GetCorner(int x, int y, int z){
        return Corners[x+y*2+z*4];
    }
    public void SetCorner(int x, int y, int z,bool value){
        Corners[x+y*2+z*4]=value;
    }
}