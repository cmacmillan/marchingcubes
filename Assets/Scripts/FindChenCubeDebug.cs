﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FindChenCubeDebug : MonoBehaviour
{
    public MeshFilter mesh;
    public GameObject[] spheres;
    public Transform camParent;
    private MeshRenderer[] _sphereRendererCache;
    private MeshRenderer[] SphereRendererCache
    {
        get
        {
            if (_sphereRendererCache == null)
            {
                _sphereRendererCache = new MeshRenderer[8];
            }
            return _sphereRendererCache;
        }
    }
    private CornerConfiguration Samples;
    private MeshRenderer getMeshRenderer(int i){
        if (SphereRendererCache[i]==null){
            SphereRendererCache[i]=spheres[i].GetComponent<MeshRenderer>();
        }
        return SphereRendererCache[i];
    }
    public Material offMat;
    public Material onMat;
    public Camera cam;
    public float rotationSpeed;
    //private int currentIndex;
    void UpdateMesh()
    {
        var box = CubeMarcher.precalculatedBoxMeshes[Samples.index];
        Mesh m = new Mesh();
        m.vertices = box.Verts.Select(a=>BoxMesh.getInterpolatedPoint(a,.5f)).ToArray();
        m.triangles = box.Triangles;
        m.RecalculateNormals();
        mesh.mesh = m;
    }
    void Start()
    {
        //UpdateMesh();
        Samples = new CornerConfiguration(0);
        CubeMarcher.PrecalculateBoxMeshes();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.D)){
            var camEuler = camParent.rotation.eulerAngles;
            camParent.transform.rotation = Quaternion.Euler(camEuler.x,camEuler.y+=Time.deltaTime*rotationSpeed,camEuler.z);
        } 
        if (Input.GetKey(KeyCode.A)){
            var camEuler = camParent.rotation.eulerAngles;
            camParent.transform.rotation = Quaternion.Euler(camEuler.x,camEuler.y-=Time.deltaTime*rotationSpeed,camEuler.z);
        }
        if (Input.GetMouseButtonDown(0)){
            RaycastHit hit;
            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition),out hit)){
                var index = int.Parse(hit.collider.gameObject.name);
                Samples.Corners[index]=!Samples.Corners[index];
                getMeshRenderer(index).material = Samples.Corners[index]?onMat:offMat;
                UpdateMesh();
            }
        }
    }
}
