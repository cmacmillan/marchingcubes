﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetaballMarcher : MonoBehaviour
{
    private CubeMarcher marcher;
    private MetaballField field;
    public Vector3 extents;
    public int sampleCount;
    private Mesh mesh;
    private List<int> triangleList;
    private List<Vector3> vertList;
    private List<Vector3> normalList;
    public MeshFilter filter;
    [Range(0,5)]
    public float threshold;
    public List<GameObject> metaballs;
    void Start()
    {
        field = new MetaballField();
        field.spheres = new Vector3[5];
        mesh = new Mesh();
        filter.mesh = mesh;
        vertList = new List<Vector3>(1000);
        triangleList = new List<int>(1000);
        normalList = new List<Vector3>(1000);
        marcher = new CubeMarcher(field,this.transform.position,extents,sampleCount);
        CubeMarcher.PrecalculateBoxMeshes();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i=0;i<metaballs.Count;i++){
            field.spheres[i]=metaballs[i].transform.position;
        }
        /*field.spheres.Clear();
        foreach (var i in metaballs){
            field.spheres.Add(i.transform.position);
        }*/
        //field.threshold = threshold;
        //var m = new Mesh();
        filter.mesh.Clear();
        marcher.getMesh(filter.mesh,triangleList,vertList,normalList,Vector3.one,threshold);
        //filter.mesh = m;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(this.transform.position,this.extents);
    }
}


