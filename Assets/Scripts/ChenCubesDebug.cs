﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChenCubesDebug : MonoBehaviour
{
    public MeshFilter mesh;
    public GameObject[] spheres;
    public int currentIndex=0;
    void UpdateMesh()
    {
        for (int i = 0; i < spheres.Length; i++)
        {
            spheres[i].SetActive(BoxMesh.ChernyaevCubes[currentIndex].Corners.Corners[i]);
        }
        Mesh m = new Mesh();
        var verts = BoxMesh.ChernyaevCubes[currentIndex].Verts.ToList();
        m.vertices = verts.Select(a=>BoxMesh.getInterpolatedPoint(a,.5f)).ToArray();
        m.triangles = BoxMesh.ChernyaevCubes[currentIndex].Triangles;
        m.RecalculateNormals();
        mesh.mesh = m;
    }
    void Start(){
        UpdateMesh();
    }
    void Update()
    {
        var oldIndex = currentIndex;
       if (Input.GetKeyDown(KeyCode.LeftArrow)) {
           currentIndex--;
       }
       if (Input.GetKeyDown(KeyCode.RightArrow)){
           currentIndex++;
       }
       if (Input.GetKeyDown(KeyCode.Z)){
           BoxMesh.ChernyaevCubes[currentIndex].rotateCounterClockwiseAroundZ();
           UpdateMesh();
       }
       if (Input.GetKeyDown(KeyCode.X)){
           BoxMesh.ChernyaevCubes[currentIndex].rotateCounterClockwiseAroundX();
           UpdateMesh();
       }
       if (Input.GetKeyDown(KeyCode.C)){
           BoxMesh.ChernyaevCubes[currentIndex].rotateCounterClockwiseAroundY();
           UpdateMesh();
       }
       if (currentIndex<0){
           currentIndex+=BoxMesh.ChernyaevCubes.Length;
       }
       currentIndex%=BoxMesh.ChernyaevCubes.Length;
       if (currentIndex!=oldIndex){
            UpdateMesh();
        }
    }
}
