using System.Collections.Generic;
using UnityEngine;

public interface ISampleableField
{
    float sampleAtPoint(Vector3 position);
}
public class CircleField : ISampleableField
{
    public Vector3 position;
    public float sampleAtPoint(Vector3 position)
    {
        return Vector3.Distance(position,this.position);
    }
}
public class MetaballField : ISampleableField
{
    public Vector3[] spheres;
    private float falloffCurve(Vector3 position){
        return 1/(position.x*position.x+position.y*position.y+position.z*position.z);
        //float val = 1.0f-(Mathf.Clamp01(position.sqrMagnitude));//position.x*position.x+position.y*position.y+position.z*position.z));
        //return val*val;
    }
    public float sampleAtPoint(Vector3 position)
    {
        float sum = 0;
        for (int i=0;i<spheres.Length;i++){
            sum+=falloffCurve(position-spheres[i]);
        }
        return sum;
    }
}