using UnityEngine;

public enum BoxEdge{
    from000to100=1,//new Vector3(.5f,0,0)
    from000to010=2,//new Vector3(0,.5f,0)
    from000to001=4,//new Vector3(0,0,.5f)
    from100to110=8,//new Vector3(1,.5f,0)
    from100to101=16,//new Vector3(1,0,.5f)
    from010to110=32,//new Vector3(.5f,1,0)
    from010to011=64,//new Vector3(0,1,.5f)
    from001to101=128,//new Vector3(.5f,0,1)
    from001to011=256,//new Vector3(0,.5f,1)
    from011to111=512,//new Vector3(.5f,1,1)
    from110to111=1024,//new Vector3(1,1,.5f)
    from101to111=2048//new Vector3(1,.5f,1)
}
public class BoxMesh
{
    public static readonly int bottomFace = ((int)BoxEdge.from000to100)|((int)BoxEdge.from000to001)|((int)BoxEdge.from100to101)|((int)BoxEdge.from001to101);
    public static readonly int leftFace = ((int)BoxEdge.from000to010)|((int)BoxEdge.from010to011)|((int)BoxEdge.from001to011)|((int)BoxEdge.from000to001);
    public static readonly int nearFace = ((int)BoxEdge.from000to100)|((int)BoxEdge.from100to110)|((int)BoxEdge.from010to110)|((int)BoxEdge.from000to010);
    public static BoxEdge getLeftTwinEdge(BoxEdge edge){
        switch (edge){
            case BoxEdge.from000to010:
                return BoxEdge.from100to110;
            case BoxEdge.from010to011:
                return BoxEdge.from110to111;
            case BoxEdge.from001to011:
                return BoxEdge.from101to111;
            case BoxEdge.from000to001:
                return BoxEdge.from100to101;
            default:
                throw new System.Exception("BAD");
        }
    }
    public static BoxEdge getNearTwinEdge(BoxEdge edge){
        switch (edge){
            case BoxEdge.from000to100:
                return BoxEdge.from001to101;
            case BoxEdge.from100to110:
                return BoxEdge.from101to111;
            case BoxEdge.from010to110:
                return BoxEdge.from011to111;
            case BoxEdge.from000to010:
                return BoxEdge.from001to011;
            default:
                throw new System.Exception("BAD");
        }
    }
    public static BoxEdge getBottomTwinEdge(BoxEdge edge){
        switch (edge){
            case BoxEdge.from000to100:
                return BoxEdge.from010to110;
            case BoxEdge.from000to001:
                return BoxEdge.from010to011;
            case BoxEdge.from100to101:
                return BoxEdge.from110to111;
            case BoxEdge.from001to101:
                return BoxEdge.from011to111;
            default:
                throw new System.Exception("BAD");
        }
    }
    public static Vector3 getInterpolatedPoint(BoxEdge edge,float lerpValue){
        switch (edge)
        {
            case BoxEdge.from000to100:
                return new Vector3(lerpValue,0,0);
            case BoxEdge.from000to010:
                return new Vector3(0,lerpValue,0);
            case BoxEdge.from000to001:
                return new Vector3(0,0,lerpValue);
            case BoxEdge.from100to110:
                return new Vector3(1,lerpValue,0);
            case BoxEdge.from100to101:
                return new Vector3(1,0,lerpValue);
            case BoxEdge.from010to110:
                return new Vector3(lerpValue,1,0);
            case BoxEdge.from010to011:
                return new Vector3(0,1,lerpValue);
            case BoxEdge.from001to101:
                return new Vector3(lerpValue,0,1);
            case BoxEdge.from001to011:
                return new Vector3(0,lerpValue,1);
            case BoxEdge.from011to111:
                return new Vector3(lerpValue,1,1);
            case BoxEdge.from110to111:
                return new Vector3(1,1,lerpValue);
            case BoxEdge.from101to111:
                return new Vector3(1,lerpValue,1);
            default:
                throw new System.Exception("oops never should get here");
        }
    }
    public CornerConfiguration Corners;
    public BoxEdge[] Verts;
    public int[] Triangles;
    public bool inverted;

    public void Invert(){
        inverted=!inverted;
        for (int i=0;i<Triangles.Length;i+=3){
            var val = Triangles[i];
            Triangles[i]=Triangles[i+2];
            Triangles[i+2]=val;
        }
        Corners.Invert();
    }
    
    public void rotateCounterClockwiseAroundX(){
        for (int i=0;i<Verts.Length;i++){
            BoxEdge vert;
            switch(Verts[i]){
                //left face
                case BoxEdge.from000to010:
                    vert = BoxEdge.from010to011;
                    break;
                case BoxEdge.from010to011:
                    vert = BoxEdge.from001to011;
                    break;
                case BoxEdge.from001to011:
                    vert = BoxEdge.from000to001;
                    break;
                case BoxEdge.from000to001:
                    vert = BoxEdge.from000to010;
                    break;
                //right face, just set x to 1
                case BoxEdge.from100to110:
                    vert = BoxEdge.from110to111;
                    break;
                case BoxEdge.from110to111:
                    vert = BoxEdge.from101to111;
                    break;
                case BoxEdge.from101to111:
                    vert = BoxEdge.from100to101;
                    break;
                case BoxEdge.from100to101:
                    vert = BoxEdge.from100to110;
                    break;
                //conncting rods
                case BoxEdge.from000to100:
                    vert = BoxEdge.from010to110;
                    break;
                case BoxEdge.from010to110:
                    vert = BoxEdge.from011to111;
                    break;
                case BoxEdge.from011to111:
                    vert = BoxEdge.from001to101;
                    break;
                case BoxEdge.from001to101:
                    vert = BoxEdge.from000to100;
                    break;
                default:
                    vert=Verts[i];
                    break;
            }
            Verts[i]=vert;
        }
        Corners.rotateCounterClockwiseAroundX();
    }
    public void rotateCounterClockwiseAroundY(){
        for (int i=0;i<Verts.Length;i++){
            BoxEdge vert;
            switch(Verts[i]){
                //bottom face
                case BoxEdge.from000to100:
                    vert = BoxEdge.from100to101;
                    break;
                case BoxEdge.from100to101:
                    vert = BoxEdge.from001to101;
                    break;
                case BoxEdge.from001to101:
                    vert = BoxEdge.from000to001;
                    break;
                case BoxEdge.from000to001:
                    vert = BoxEdge.from000to100;
                    break;
                //top face, just setting y to 1
                case BoxEdge.from010to110:
                    vert = BoxEdge.from110to111;
                    break;
                case BoxEdge.from110to111:
                    vert = BoxEdge.from011to111;
                    break;
                case BoxEdge.from011to111:
                    vert = BoxEdge.from010to011;
                    break;
                case BoxEdge.from010to011:
                    vert = BoxEdge.from010to110;
                    break;
                //connecting rods
                case BoxEdge.from000to010:
                    vert = BoxEdge.from100to110;
                    break;
                case BoxEdge.from100to110:
                    vert = BoxEdge.from101to111;
                    break;
                case BoxEdge.from101to111:
                    vert = BoxEdge.from001to011;
                    break;
                case BoxEdge.from001to011:
                    vert = BoxEdge.from000to010;
                    break;
                default:
                    vert=Verts[i];
                    break;
            }
            Verts[i]=vert;
        }
        Corners.rotateCounterClockwiseAroundY();
    }
    public void rotateCounterClockwiseAroundZ(){
        for (int i=0;i<Verts.Length;i++){
            BoxEdge vert;
            switch(Verts[i]){
                //Near face
                case BoxEdge.from000to100:
                    vert=BoxEdge.from100to110;
                    break;
                case BoxEdge.from100to110:
                    vert=BoxEdge.from010to110;
                    break;
                case BoxEdge.from010to110:
                    vert=BoxEdge.from000to010;
                    break;
                case BoxEdge.from000to010:
                    vert = BoxEdge.from000to100;
                    break;
                //far face, z just gets set to 1
                case BoxEdge.from001to101:
                    vert=BoxEdge.from101to111;
                    break;
                case BoxEdge.from101to111:
                    vert=BoxEdge.from011to111;
                    break;
                case BoxEdge.from011to111:
                    vert=BoxEdge.from001to011;
                    break;
                case BoxEdge.from001to011:
                    vert = BoxEdge.from001to101;
                    break;
                //connecting rods
                case BoxEdge.from000to001:
                    vert = BoxEdge.from100to101;
                    break;
                case BoxEdge.from100to101:
                    vert = BoxEdge.from110to111;
                    break;
                case BoxEdge.from110to111:
                    vert = BoxEdge.from010to011;
                    break;
                case BoxEdge.from010to011:
                    vert = BoxEdge.from000to001;
                    break;
                default:
                    vert = Verts[i];
                    break;
            }
            Verts[i] = vert;
        }
        Corners.rotateCounterClockwiseAroundZ();
    }
    public BoxMesh Clone(){
        //Vector3[] cloneVerts = (Vector3[])Verts.Clone();
        BoxEdge[] cloneVerts = (BoxEdge[])Verts.Clone();
        int[] cloneTriangles = (int[])Triangles.Clone();
        var clone = new BoxMesh(Corners.index,cloneVerts,cloneTriangles);
        clone.inverted = inverted;
        return clone;
    }

    public BoxMesh(int cornerConfig,BoxEdge[] Verts, int[] Triangles){
        inverted=false;
        this.Verts = Verts;
        this.Triangles = Triangles;
        this.Corners = new CornerConfiguration(cornerConfig);
    }

    public static BoxMesh[] ChernyaevCubes = {
        ///           76543210
        new BoxMesh(0b00000000,new BoxEdge[0],new int[0]),
        new BoxMesh(0b00000001,new BoxEdge[]{
                                    BoxEdge.from000to100,
                                    BoxEdge.from000to010,
                                    BoxEdge.from000to001},
                               new int[]{
                                   0,1,2,
                               }),
        new BoxMesh(0b00000011,new BoxEdge[]{
                                    BoxEdge.from100to101,
                                    BoxEdge.from100to110,
                                    BoxEdge.from000to010,
                                    BoxEdge.from000to001,
                               },
                               new int[]{
                                   0,1,2,
                                   0,2,3,
                               }),
        new BoxMesh(0b00001001,new BoxEdge[]{
                                    BoxEdge.from000to100,
                                    BoxEdge.from000to010,
                                    BoxEdge.from000to001,
                                    BoxEdge.from100to110,
                                    BoxEdge.from010to110,
                                    BoxEdge.from110to111,
                               },
                               new int[]{
                                   0,1,2,
                                   5,4,3
                               }),
        new BoxMesh(0b00110010,new BoxEdge[]{
                                    BoxEdge.from000to001,
                                    BoxEdge.from100to110,
                                    BoxEdge.from000to100,
                                    BoxEdge.from001to011,
                                    BoxEdge.from101to111,
                                },
                                new int[]{
                                    0,1,2,
                                    0,3,1,
                                    3,4,1,
                                }),
        new BoxMesh(0b00110011,new BoxEdge[]{
                                    BoxEdge.from000to010,
                                    BoxEdge.from101to111,
                                    BoxEdge.from100to110,
                                    BoxEdge.from001to011,
                                },new int[]{
                                    0,1,2,
                                    0,3,1
                                }),
        new BoxMesh(0b00110110,new BoxEdge[]{
                                    BoxEdge.from000to001,
                                    BoxEdge.from100to110,
                                    BoxEdge.from000to100,
                                    BoxEdge.from001to011,
                                    BoxEdge.from101to111,
                                    BoxEdge.from000to010,
                                    BoxEdge.from010to011,
                                    BoxEdge.from010to110,
                                },new int[]{
                                    0,1,2,
                                    0,3,1,
                                    3,4,1,
                                    7,6,5,
                                }),
        new BoxMesh(0b01101001,new BoxEdge[]{
                                    BoxEdge.from000to100,
                                    BoxEdge.from000to010,
                                    BoxEdge.from000to001,
                                    BoxEdge.from100to110,
                                    BoxEdge.from010to110,
                                    BoxEdge.from110to111,
                                    BoxEdge.from010to011,
                                    BoxEdge.from011to111,
                                    BoxEdge.from001to011,
                                    BoxEdge.from100to101,
                                    BoxEdge.from001to101,
                                    BoxEdge.from101to111,
                              },new int[]{
                                   0,1,2,
                                   5,4,3,
                                   6,7,8,
                                   9,10,11
                              }),
        new BoxMesh(0b01110001,new BoxEdge[]{
                                BoxEdge.from000to010,
                                BoxEdge.from010to011,
                                BoxEdge.from011to111,
                                BoxEdge.from000to100,
                                BoxEdge.from101to111,
                                BoxEdge.from100to101,
                               },new int[]{
                                    0,1,2,
                                    0,2,3,
                                    3,2,4,
                                    3,4,5
                               }),
        new BoxMesh(0b01110010,new BoxEdge[]{
                                BoxEdge.from000to001,
                                BoxEdge.from010to011,
                                BoxEdge.from000to100,
                                BoxEdge.from101to111,
                                BoxEdge.from011to111,
                                BoxEdge.from100to110,
                              },new int[]{
                                0,1,2,
                                1,3,2,
                                1,4,3,
                                2,3,5
                              }),
        new BoxMesh(0b10000001,new BoxEdge[]{
                                BoxEdge.from000to100,
                                BoxEdge.from000to010,
                                BoxEdge.from000to001,
                                BoxEdge.from110to111,
                                BoxEdge.from011to111,
                                BoxEdge.from101to111,
                             },new int[]{
                                0,1,2,
                                5,4,3
                             }),
        new BoxMesh(0b10000011,new BoxEdge[]{
                                    BoxEdge.from100to101,
                                    BoxEdge.from100to110,
                                    BoxEdge.from000to010,
                                    BoxEdge.from000to001,
                                    BoxEdge.from110to111,
                                    BoxEdge.from011to111,
                                    BoxEdge.from101to111,
                              },new int[]{
                                   0,1,2,
                                   0,2,3,
                                   6,5,4
                              }),
        new BoxMesh(0b10000110,new BoxEdge[]{
                                    BoxEdge.from000to010,
                                    BoxEdge.from010to110,
                                    BoxEdge.from010to011,
                                    BoxEdge.from000to100,
                                    BoxEdge.from100to101,
                                    BoxEdge.from100to110,
                                    BoxEdge.from011to111,
                                    BoxEdge.from110to111,
                                    BoxEdge.from101to111,
                                }, new int[]{
                                    0,1,2,
                                    3,4,5,
                                    6,7,8
                                }),
        new BoxMesh(0b10100101,new BoxEdge[]{
                                    BoxEdge.from000to100,
                                    BoxEdge.from000to001,
                                    BoxEdge.from010to011,
                                    BoxEdge.from010to110,
                                    BoxEdge.from001to101,
                                    BoxEdge.from011to111,
                                    BoxEdge.from100to101,
                                    BoxEdge.from110to111,
                                },new int[]{
                                    2,1,0,
                                    3,2,0,
                                    4,5,6,
                                    5,7,6
                                }),
        new BoxMesh(0b10110001,new BoxEdge[]{
                                    BoxEdge.from000to100,
                                    BoxEdge.from110to111,
                                    BoxEdge.from100to101,
                                    BoxEdge.from001to011,
                                    BoxEdge.from011to111,
                                    BoxEdge.from000to010,
                                },new int[]{
                                    0,1,2,
                                    0,3,1,
                                    3,4,1,
                                    5,3,0
                                }),
    };
}