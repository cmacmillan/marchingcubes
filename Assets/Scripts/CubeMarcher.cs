﻿using System.Collections;
using System.Collections.Generic;
using Torec;
using UnityEngine;
//using System.Numerics;

public class CubeMarcher
{
    private int sampleWidth {get {return (int)(extents.x*sampleDensity);}}
    private int cachedSampleWidth;
    private int cachedSampleHeight;
    private int cachedSampleDepth;
    private int sampleHeight {get {return (int)(extents.y*sampleDensity);}}
    private int sampleDepth {get {return (int)(extents.z*sampleDensity);}}
    private int boxCountWidth {get {return sampleWidth-1;}}
    private int cachedBoxCountWidth;
    private int cachedBoxCountHeight;
    private int cachedBoxCountDepth;
    private int boxCountHeight {get {return sampleHeight-1;}}
    private int boxCountDepth {get {return sampleDepth-1;}}
    private ISampleableField field;
    public Vector3 center;
    public Vector3 extents;
    private Vector3 startPoint { get {return center-extents/2.0f;}}
    private Vector3 cachedStartPoint;
    public float sampleDensity;
    private float[][][] cornerPoints;
    private BoxMesh[][][] runtimeCalculatedBoxMeshes;
    private int[][][][] boxMeshVertexOffsets;
    public static BoxMesh[] precalculatedBoxMeshes;
    private void initCornerPoints(){
        cachedSampleWidth = sampleWidth;
        cachedSampleHeight = sampleHeight;
        cachedSampleDepth = sampleDepth;
        cachedStartPoint = startPoint;
        cachedBoxCountWidth = boxCountWidth;
        cachedBoxCountHeight = boxCountHeight;
        cachedBoxCountDepth = boxCountDepth;
        cornerPoints = new float[sampleWidth][][];
        for (int x=0;x<sampleWidth;x++){
            cornerPoints[x] = new float[sampleHeight][];
            for (int y=0;y<sampleHeight;y++){
                cornerPoints[x][y] = new float[sampleDepth]; 
            }
        }
        runtimeCalculatedBoxMeshes = new BoxMesh[sampleWidth][][];
        for (int x=0;x<sampleWidth;x++){
            runtimeCalculatedBoxMeshes[x] = new BoxMesh[sampleHeight][];
            for (int y=0;y<sampleHeight;y++){
                runtimeCalculatedBoxMeshes[x][y] = new BoxMesh[sampleDepth];
            }
        }
        boxMeshVertexOffsets = new int[sampleWidth][][][];
        for (int x=0;x<sampleWidth;x++){
            boxMeshVertexOffsets[x] = new int[sampleHeight][][];
            for (int y=0;y<sampleHeight;y++){
                boxMeshVertexOffsets[x][y] = new int[sampleDepth][];
                for (int z=0;z<sampleDepth;z++){
                    boxMeshVertexOffsets[x][y][z] = new int[12];
                }
            }
        }
    }

    private enum BoxRotation{
        none,
        x,
        y,
        z,
    }
    private static BoxMesh AddBoxMesh(BoxMesh box,BoxRotation rotation=BoxRotation.none){
        var clone = box.Clone();
        switch (rotation){
            case BoxRotation.x:
                clone.rotateCounterClockwiseAroundX();
                break;
            case BoxRotation.y:
                clone.rotateCounterClockwiseAroundY();
                break;
            case BoxRotation.z:
                clone.rotateCounterClockwiseAroundZ();
                break;
        }
        if (precalculatedBoxMeshes[clone.Corners.index] == null || precalculatedBoxMeshes[clone.Corners.index].inverted)
        {
            precalculatedBoxMeshes[clone.Corners.index] = clone;
        }
        var inverted = clone.Clone();
        inverted.Invert();
        if (precalculatedBoxMeshes[inverted.Corners.index] == null){
            precalculatedBoxMeshes[inverted.Corners.index] = inverted;
        }
        return clone;
    }
    public static void PrecalculateBoxMeshes(){
        precalculatedBoxMeshes = new BoxMesh[256];
        var possibleItems = new List<BoxMesh>();
        for (int i=0;i<BoxMesh.ChernyaevCubes.Length;i++){
            var currentCube = BoxMesh.ChernyaevCubes[i];
            //first face
            var box = AddBoxMesh(currentCube);
            box = AddBoxMesh(box,BoxRotation.y);
            box = AddBoxMesh(box,BoxRotation.y);
            box = AddBoxMesh(box,BoxRotation.y);
            //second face
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.x);
            //third face
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.y);
            box = AddBoxMesh(box,BoxRotation.y);
            box = AddBoxMesh(box,BoxRotation.y);
            //fourth face
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.x);
            //back to start
            box = AddBoxMesh(box,BoxRotation.z);
            //fifth face
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.z);
            //final face
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.x);
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.z);
            box = AddBoxMesh(box,BoxRotation.z);
        }
        Debug.Log("finished precalculation");
        for (int i=0;i<256;i++){
            if (precalculatedBoxMeshes[i]==null){
                Debug.Log("index '"+i+"' was missing a bake");
            }
        }
    }
    public CubeMarcher(ISampleableField field, Vector3 center, Vector3 extents, float sampleDensity){
        this.field = field;
        this.center = center;
        this.extents = extents;
        this.sampleDensity = sampleDensity;
        initCornerPoints();
    }
    private void CalculateCornerPoints(){
        float invSampleDensity = 1/sampleDensity;
        Vector3 currentPosition = new Vector3();
        for (int x=0;x<cachedSampleWidth;x++)
        {
            for (int y = 0; y < cachedSampleHeight; y++)
            {
                for (int z = 0; z < cachedSampleDepth; z++)
                {
                    currentPosition.Set(cachedStartPoint.x+x*invSampleDensity,
                                        cachedStartPoint.y+y*invSampleDensity,
                                        cachedStartPoint.z+z*invSampleDensity);
                    //Vector3 currentPosition = startPoint+new Vector3(x,y,z)*invSampleDensity;
                    cornerPoints[x][y][z] = field.sampleAtPoint(currentPosition);
                    //Debug.DrawRay(currentPosition,Vector3.up*.1f,cornerPoints[x][y][z]?Color.red:Color.blue);
                }
            }
        }
    }
    private float getInterpValue(int x1,int y1,int z1,int x2,int y2,int z2,float threshold){
        return (threshold - cornerPoints[x1][y1][z1]) / (cornerPoints[x2][y2][z2] - cornerPoints[x1][y1][z1]);
    }

private bool a=false;
    public void getMesh(Mesh meshToWriteTo,List<int> triangleList, List<Vector3> vertList,List<Vector3> normalList,Vector3 sphereCenter,float threshold){
        //if (!a){
            CalculateCornerPoints();
            //a=true;
        //}
        triangleList.Clear();
        vertList.Clear();
        normalList.Clear();
        CornerConfiguration config = new CornerConfiguration(0);
        Vector3 currentPosition= new Vector3();
        float invSampleDensity = 1/sampleDensity;
        float extentsOverSampleWidthX = extents.x/sampleWidth;
        float extentsOverSampleWidthY = extents.y/sampleWidth;
        float extentsOverSampleWidthZ = extents.z/sampleWidth;
        var halfExtents = extents/2;
        for (int x=0;x<cachedBoxCountWidth;x++)
        {
            for (int y = 0; y < cachedBoxCountHeight; y++)
            {
                for (int z = 0; z < cachedBoxCountDepth; z++)
                {
                    currentPosition.Set(
                        (x+.5f)*invSampleDensity,
                        (y+.5f)*invSampleDensity,
                        (z+.5f)*invSampleDensity
                    );
                    config.cachedIndex=0;
                    config.cachedIndex|=cornerPoints[x][y][z]>threshold?1:0;
                    config.cachedIndex|=cornerPoints[x+1][y][z]>threshold?2:0;
                    config.cachedIndex|=cornerPoints[x][y+1][z]>threshold?4:0;
                    config.cachedIndex|=cornerPoints[x+1][y+1][z]>threshold?8:0;

                    config.cachedIndex|=cornerPoints[x][y][z+1]>threshold?16:0;
                    config.cachedIndex|=cornerPoints[x+1][y][z+1]>threshold?32:0;
                    config.cachedIndex|=cornerPoints[x][y+1][z+1]>threshold?64:0;
                    config.cachedIndex|=cornerPoints[x+1][y+1][z+1]>threshold?128:0;

                    var BoxMesh = precalculatedBoxMeshes[config.cachedIndex];
                    runtimeCalculatedBoxMeshes[x][y][z]=BoxMesh;
                    int vertexCount = vertList.Count;
                    //boxMeshVertexOffsets[x][y][z] = vertexCount;
                    for (int i=0;i<BoxMesh.Verts.Length;i++){
                        var edge = BoxMesh.Verts[i];
                        Vector3 vert;
                        float interpolationValue;
                        {
                            if ((BoxMesh.nearFace&(int)edge)>0 && z>0)//we share a vert with z-1
                            {
                                var twin = BoxMesh.getNearTwinEdge(edge);
                                var zMinusBoxMesh = runtimeCalculatedBoxMeshes[x][y][z-1];
                                int len = zMinusBoxMesh.Verts.Length;
                                for (int c = 0; c < len; c++)
                                {
                                    if (zMinusBoxMesh.Verts[c] == twin)
                                    {
                                        boxMeshVertexOffsets[x][y][z][i]=boxMeshVertexOffsets[x][y][z-1][c];
                                    }
                                }
                                vert = Vector3.zero;
                            }
                            else if ((BoxMesh.bottomFace & (int)edge) > 0 && y > 0)
                            {//we share a vert with y-1
                                var twin = BoxMesh.getBottomTwinEdge(edge);
                                var yMinusBoxMesh = runtimeCalculatedBoxMeshes[x][y-1][z];
                                int len = yMinusBoxMesh.Verts.Length;
                                for (int c = 0; c < len; c++)
                                {
                                    if (yMinusBoxMesh.Verts[c] == twin)
                                    {
                                        boxMeshVertexOffsets[x][y][z][i]=boxMeshVertexOffsets[x][y-1][z][c];
                                    }
                                }
                                vert = Vector3.zero;
                            }
                            else if ((BoxMesh.leftFace & (int)edge) > 0 && x > 0)
                            {//we share a vert with x-1
                                var twin = BoxMesh.getLeftTwinEdge(edge);
                                var xMinusBoxMesh = runtimeCalculatedBoxMeshes[x-1][y][z];
                                int len = xMinusBoxMesh.Verts.Length;
                                for (int c = 0; c < len; c++)
                                {
                                    if (xMinusBoxMesh.Verts[c] == twin)
                                    {
                                        boxMeshVertexOffsets[x][y][z][i]=boxMeshVertexOffsets[x-1][y][z][c];
                                    }
                                }
                                vert = Vector3.zero;
                            }
                            else
                            {
                                boxMeshVertexOffsets[x][y][z][i]=i+vertexCount;
                                switch (edge)
                                {
                                    case BoxEdge.from000to100:
                                        interpolationValue = getInterpValue(x, y, z, x + 1, y, z, threshold);
                                        break;
                                    case BoxEdge.from000to010:
                                        interpolationValue = getInterpValue(x, y, z, x, y + 1, z, threshold);
                                        break;
                                    case BoxEdge.from000to001:
                                        interpolationValue = getInterpValue(x, y, z, x, y, z + 1, threshold);
                                        break;
                                    case BoxEdge.from100to110:
                                        interpolationValue = getInterpValue(x + 1, y, z, x + 1, y + 1, z, threshold);
                                        break;
                                    case BoxEdge.from100to101:
                                        interpolationValue = getInterpValue(x + 1, y, z, x + 1, y, z + 1, threshold);
                                        break;
                                    case BoxEdge.from010to110:
                                        interpolationValue = getInterpValue(x, y + 1, z, x + 1, y + 1, z, threshold);
                                        break;
                                    case BoxEdge.from010to011:
                                        interpolationValue = getInterpValue(x, y + 1, z, x, y + 1, z + 1, threshold);
                                        break;
                                    case BoxEdge.from001to101:
                                        interpolationValue = getInterpValue(x, y, z + 1, x + 1, y, z + 1, threshold);
                                        break;
                                    case BoxEdge.from001to011:
                                        interpolationValue = getInterpValue(x, y, z + 1, x, y + 1, z + 1, threshold);
                                        break;
                                    case BoxEdge.from011to111:
                                        interpolationValue = getInterpValue(x, y + 1, z + 1, x + 1, y + 1, z + 1, threshold);
                                        break;
                                    case BoxEdge.from110to111:
                                        interpolationValue = getInterpValue(x + 1, y + 1, z, x + 1, y + 1, z + 1, threshold);
                                        break;
                                    case BoxEdge.from101to111:
                                        interpolationValue = getInterpValue(x + 1, y, z + 1, x + 1, y + 1, z + 1, threshold);
                                        break;
                                    default:
                                        interpolationValue = .5f;
                                        break;
                                }
                                vert = BoxMesh.getInterpolatedPoint(BoxMesh.Verts[i], interpolationValue) * invSampleDensity + new Vector3(
                                    x * extentsOverSampleWidthX,
                                    y * extentsOverSampleWidthY,
                                    z * extentsOverSampleWidthZ
                                ) - halfExtents;
                            }
                        }
                        vertList.Add(vert);
                        //normalList.Add((vert-sphereCenter));
                    }
                    for (int i=0;i<BoxMesh.Triangles.Length;i++){
                        triangleList.Add(boxMeshVertexOffsets[x][y][z][BoxMesh.Triangles[i]]);
                    }
                    //need to remove duplicate verts
                }
            }
        }
        meshToWriteTo.vertices = vertList.ToArray();
        meshToWriteTo.triangles = triangleList.ToArray();
        //meshToWriteTo.normals = normalList.ToArray();
        //CatmullClark.Subdivide(meshToWriteTo,2);
        meshToWriteTo.RecalculateNormals();
    }
}
