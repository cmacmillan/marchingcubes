﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageCircleFieldBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public float radius;
    private CubeMarcher marcher;
    private CircleField field;
    public Vector3 extents;
    public int sampleCount;
    private Mesh mesh;
    private List<int> triangleList;
    private List<Vector3> vertList;
    private List<Vector3> normalList;
    public GameObject sphere;
    public MeshFilter filter;
    public MeshRenderer marchedFilter;
    public MeshRenderer defaultMesh;
    void Start()
    {
        field = new CircleField();
        mesh = new Mesh();
        filter.mesh = mesh;
        vertList = new List<Vector3>(1000);
        triangleList = new List<int>(1000);
        normalList = new List<Vector3>(1000);
        marcher = new CubeMarcher(field,this.transform.position,extents,sampleCount);
        CubeMarcher.PrecalculateBoxMeshes();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)){
            marchedFilter.enabled=false;
            defaultMesh.enabled = true;
        } else {
            marchedFilter.enabled=true;
            defaultMesh.enabled = false;
        }
        //field.radius=radius;
        field.position = sphere.transform.position;//this.transform.position;
        var m = new Mesh();
        //removing the line below bricks this
        //marcher.getMesh(m,triangleList,vertList,normalList,sphere.transform.position-this.transform.position);
        //mesh.UploadMeshData(false);
        filter.mesh = m;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(this.transform.position,this.extents);
    }
}
